# CV Builder

This Web application helps you to build your CV (or resume). You can enter your personal data, education and work experience in a form. The data is stored in a database. Finally you get your CV as a PDF file. 

## Installation

N/A

## Usage

In terminal:
```
cd server
npm run dev
```
In another terminal:
```
cd client
npm start
```

## Contributing

...

## License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

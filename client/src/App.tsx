import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import MainForm from "./components/MainForm";
import { BrowserRouter as Router, Route } from "react-router-dom";
import LogIn from "./components/LogIn";
import CVView from "./components/CVView";

function App() {
    const [email, setEmail] = useState("");
    const [pdfUrl, setPdfUrl] = useState<string | null>("");

    return (
        <div className="App">
            <div className="container">
                <div className="row">
                    <div className="col-2"></div>
                    <div className="col-8">
                        <h1>CV Builder</h1>
                        <Router>
                            <Route
                                exact
                                path="/"
                                render={() => <LogIn onLogin={setEmail} />}
                            />
                            <Route
                                exact
                                path="/cv-form"
                                render={() => <MainForm email={email} setPdfUrl={setPdfUrl} />}
                            />
                            <Route
                                exact
                                path="/show-cv"
                                render={() => <CVView pdfUrl={pdfUrl} />}
                            />
                        </Router>
                    </div>
                    <div className="col-2"></div>
                </div>
            </div>
        </div>
    );
}

export default App;

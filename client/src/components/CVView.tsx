import React from "react";

interface ICVView {
    pdfUrl: string | null;
}

const CVView: React.FC<ICVView> = (props) => {
    const { pdfUrl } = props;

    const handleViewPDFClick = () => {
        if (pdfUrl) {
            window.open(pdfUrl);
        }
    };

    return (
        <div>
            Your CV was saved.
            <br />
            <button type="button" className="btn btn-primary mt-3" onClick={handleViewPDFClick}>
                View/Download your CV as PDF
            </button>
        </div>
    );
};

export default CVView;

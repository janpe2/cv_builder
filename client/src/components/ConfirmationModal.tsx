import React from "react";
import Modal from "react-modal";

interface IConfirmationModal {
    message: string;
    title: string;
    okButtonText: string;
    isOpen: boolean;
    onOKClick: () => void;
    onCancelClick: () => void;
}

const ConfirmationModal: React.FC<IConfirmationModal> = (props) => {
    const {
        message,
        title,
        isOpen,
        onOKClick,
        onCancelClick,
        okButtonText,
    } = props;
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onCancelClick}
            className="modal-content"
            contentLabel="Confirmation Modal"
            ariaHideApp={false}
            shouldCloseOnOverlayClick={false}
        >
            <h2 className="header">{title}</h2>
            <p>{message}</p>
            <div className="buttons-div mt-2">
                <button
                    type="button"
                    onClick={onOKClick}
                    className={okButtonText === "Delete" ? "btn btn-danger" : "btn btn-primary"}
                >
                    {okButtonText}
                </button>{" "}
                <button
                    type="button"
                    onClick={onCancelClick}
                    className="btn btn-secondary"
                >
                    Cancel
                </button>
            </div>
        </Modal>
    );
};

export default ConfirmationModal;

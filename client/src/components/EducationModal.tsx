import React, { useState } from "react";
import Modal from "react-modal";
import { IEducation } from "../types";
import "./WorkExperienceModal.css";
import { Formik, Field, Form, ErrorMessage, FormikHelpers } from "formik";
import FormValidationError from "./FormValidationError";

interface IEducationModal {
    isOpen: boolean;
    setHidden: () => void;
    educationForEditing: IEducation | null;
    addEducation: (work: IEducation, isEditing: boolean) => void;
    getEducationKey: () => string;
}

interface IEducationFormValues {
    degreeName: string;
    schoolName: string;
    year: number;
    description: string;
}

const EducationModal: React.FC<IEducationModal> = (props) => {
    const {
        isOpen,
        setHidden,
        addEducation,
        getEducationKey,
        educationForEditing,
    } = props;
    const [formValues, setFormValues] = useState<IEducationFormValues>({
        degreeName: "",
        schoolName: "",
        year: 2000,
        description: "",
    });

    const afterOpenModal = () => {
        setFormValues(
            educationForEditing
                ? {
                      degreeName: educationForEditing.degreeName,
                      schoolName: educationForEditing.schoolName,
                      year: educationForEditing.year,
                      description: educationForEditing.description,
                  }
                : {
                      degreeName: "",
                      schoolName: "",
                      year: new Date().getFullYear(),
                      description: "",
                  }
        );
    };

    const submitEducation = (
        values: IEducationFormValues,
        { setSubmitting }: FormikHelpers<IEducationFormValues>
    ) => {
        const isEditing = !!educationForEditing;
        addEducation(
            {
                degreeName: values.degreeName,
                schoolName: values.schoolName,
                year: values.year,
                description: values.description,
                educationId: educationForEditing
                    ? educationForEditing.educationId
                    : getEducationKey(),
            },
            isEditing
        );
        setSubmitting(false);
        setHidden();
    };

    const validate = (values: IEducationFormValues) => {
        const errors: Partial<IEducationFormValues> = {};

        if (!values.degreeName) {
            errors.degreeName = "Degree is required";
        }

        return errors;
    };

    return (
        <Modal
            isOpen={isOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={() => setHidden()}
            className="modal-content"
            contentLabel="Add Work Experience Modal"
            ariaHideApp={false}
            shouldCloseOnOverlayClick={false}
        >
            <h2 className="header">
                {educationForEditing ? "Edit" : "Add"} Education
            </h2>

            <Formik
                initialValues={formValues}
                onSubmit={submitEducation}
                validate={validate}
                enableReinitialize
            >
                <Form>
                    <div className="mb-1">
                        <label htmlFor="edu-form-degreeName" className="form-label">
                            Degree:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="degreeName"
                            id="edu-form-degreeName"
                        />
                        <ErrorMessage name="degreeName">
                            {msg => <FormValidationError message={msg}/>}
                        </ErrorMessage>
                    </div>

                    <div className="mb-1">
                        <label htmlFor="edu-form-schoolName" className="form-label">
                            School:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="schoolName"
                            id="edu-form-schoolName"
                        />
                    </div>

                    <div className="mb-1">
                        <label htmlFor="edu-form-year" className="form-label">
                            Year:
                        </label>
                        <Field
                            type="number"
                            min={1970}
                            max={3000}
                            className="form-control form-control-sm"
                            name="year"
                            id="edu-form-year"
                        />
                    </div>

                    <div className="mb-1">
                        <label htmlFor="edu-form-description" className="form-label">
                            Description:
                        </label>
                        <Field
                            as="textarea"
                            className="form-control form-control-sm"
                            name="description"
                            placeholder="Describe your degree or education"
                            rows={4}
                            id="edu-form-description"
                        />
                    </div>

                    <div className="buttons-div mt-2">
                        <button type="submit" className="btn btn-primary">
                            {educationForEditing ? "Save" : "Add"}
                        </button>{" "}
                        <button
                            type="button"
                            onClick={() => setHidden()}
                            className="btn btn-secondary"
                        >
                            Cancel
                        </button>
                    </div>
                </Form>
            </Formik>
        </Modal>
    );
};

export default EducationModal;

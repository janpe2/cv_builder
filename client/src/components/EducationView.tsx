import React from "react";
import { IEducation } from "../types";
import { iconPencil, iconTrash } from "./Icons";

interface IEducationView {
    education: IEducation;
    educationIndex: number;
    educationKey: string;
    removeEducation: (educationKey: string) => void;
    editEducation: (educationKey: string) => void;
}

const EducationView: React.FC<IEducationView> = (props) => {
    const { education, educationIndex, educationKey, removeEducation, editEducation } = props;

    return (
        <div className="border border-secondary rounded ps-4 pe-4 pb-3 mt-1 mb-1">
            <legend>
                Degree {educationIndex + 1}{" "}
                <span
                    onClick={() => removeEducation(educationKey)}
                    title="Remove degree"
                    className="action-button"
                    style={{ color: "red" }}
                >
                    {iconTrash}
                </span>
                <span
                    onClick={() => editEducation(educationKey)}
                    title="Edit degree"
                    className="action-button"
                >
                    {iconPencil}
                </span>
            </legend>

            <dl>
                <dt>Degree:</dt>
                <dd>{education.degreeName}</dd>

                <dt>School:</dt>
                <dd>{education.schoolName}</dd>

                <dt>Year:</dt>
                <dd>{education.year}</dd>

                <dt>Description:</dt>
                <dd>{education.description}</dd>
            </dl>
        </div>
    );
};

export default EducationView;

import React from "react";
import { iconWarning } from "./Icons";

interface IFormValidationError {
    message: string;
}

const FormValidationError: React.FC<IFormValidationError> = (props) => {
    const { message } = props;
    return (
        <span style={{ color: "rgb(255, 120, 30)" }}>
            {iconWarning}
            {" "}
            {message}
        </span>
    );
};

export default FormValidationError;

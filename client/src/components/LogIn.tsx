import React from "react";
import { Formik, Field, Form, ErrorMessage, FormikHelpers } from "formik";
import FormValidationError from "./FormValidationError";
import { useHistory } from "react-router-dom";

interface ILogIn {
    onLogin: React.Dispatch<React.SetStateAction<string>>;
}

interface ILogInformValues {
    email: string;
}

const LogIn: React.FC<ILogIn> = (props) => {
    const { onLogin } = props;

    const history = useHistory();

    const validate = (values: ILogInformValues) => {
        const errors: Partial<ILogInformValues> = {};

        if (!values.email) {
            errors.email = "Email is required";
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
        ) {
            errors.email = "Invalid email address";
        }

        return errors;
    };

    const submitLogin = (
        values: ILogInformValues,
        { setSubmitting }: FormikHelpers<ILogInformValues>
    ) => {
        onLogin(values.email);
        setSubmitting(false);
        history.push("/cv-form");
    };

    return (
        <Formik
            initialValues={{
                email: "",
            }}
            onSubmit={submitLogin}
            validate={validate}
        >
            <Form className="main-form bg-light">
                <div className="mb-1">
                    <label htmlFor="login-form-email" className="form-label">
                        Email:
                    </label>
                    <Field
                        type="email"
                        className="form-control form-control-sm"
                        name="email"
                        id="login-form-email"
                        placeholder="example@domain.com"
                    />
                    <ErrorMessage name="email">
                        {(msg) => <FormValidationError message={msg} />}
                    </ErrorMessage>
                </div>
                <br />
                <button type="submit" className="btn btn-primary">
                    Log in
                </button>
            </Form>
        </Formik>
    );
};

export default LogIn;

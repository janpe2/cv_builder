import React, { useState, useRef, useEffect } from "react";
import "./MainForm.css";
import WorkExperienceView from "./WorkExperienceView";
import { IWorkExperience, IEducation, ICVData } from "../types";
import WorkExperienceModal from "./WorkExperienceModal";
import ConfirmationModal from "./ConfirmationModal";
import { Formik, Field, Form, ErrorMessage, FormikHelpers } from "formik";
import EducationModal from "./EducationModal";
import EducationView from "./EducationView";
import FormValidationError from "./FormValidationError";
import { useHistory } from "react-router-dom";
import { createPDF, getCV, saveCV } from "../functions/DataFunctions";

interface IMainForm {
    email: string;
    setPdfUrl: React.Dispatch<React.SetStateAction<string | null>>;
}

interface IMainFormValues {
    email: string;
    name: string;
    address: string;
    phone: string;
    profile: string;
}

const MainForm: React.FC<IMainForm> = (props) => {
    const { email, setPdfUrl } = props;
    const history = useHistory();

    const [workExperiences, setWorkExperiences] = useState<IWorkExperience[]>(
        []
    );
    const [isWorkExperienceModalOpen, setIsWorkExperienceModalOpen] =
        useState(false);
    const [workForEditing, setWorkForEditing] =
        useState<IWorkExperience | null>(null);
    const [workKeyForDeletion, setWorkKeyForDeletion] =
        useState<string | null>(null);

    const [educations, setEducations] = useState<IEducation[]>([]);
    const [isEduationModalOpen, setIsEducationModalOpen] = useState(false);
    const [educationForEditing, setEducationForEditing] =
        useState<IEducation | null>(null);
    const [educationKeyForDeletion, setEducationKeyForDeletion] =
        useState<string | null>(null);
    const [cvData, setCvData] = useState<ICVData | null>();

    const keyCounter = useRef(0);

    useEffect(() => {
        const loadFormData = async () => {
            const cvData = await getCV(email, keyCounter.current);
            const cv = cvData ? cvData.cv : null;
            keyCounter.current = cvData ? cvData.nextId : keyCounter.current;
            setCvData(cv);
            setWorkExperiences(cv ? cv.workExperiences : []);
            setEducations(cv ? cv.educations : []);
            console.log("CV for email", email, "is:");
            console.dir(cv);
        };
        loadFormData();
    }, [email]);

    const generateKey = () => {
        return `${keyCounter.current++}`;
    };

    const editWorkExperience = (workKey: string) => {
        const work = workExperiences.find((work) => work.workId === workKey);
        if (work) {
            setWorkForEditing(work);
            setIsWorkExperienceModalOpen(true);
        }
    };

    const editEducation = (educationKey: string) => {
        const education = educations.find(
            (edu) => edu.educationId === educationKey
        );
        if (education) {
            setEducationForEditing(education);
            setIsEducationModalOpen(true);
        }
    };

    const acceptWorkExperience = (
        editedOrNewWork: IWorkExperience,
        isEditing: boolean
    ) => {
        setWorkExperiences(
            isEditing
                ? workExperiences.map((work) =>
                      work.workId === editedOrNewWork.workId
                          ? editedOrNewWork
                          : work
                  )
                : [...workExperiences, editedOrNewWork]
        );
        setWorkForEditing(null);
    };

    const acceptEducation = (
        editedOrNewEducation: IEducation,
        isEditing: boolean
    ) => {
        setEducations(
            isEditing
                ? educations.map((edu) =>
                      edu.educationId === editedOrNewEducation.educationId
                          ? editedOrNewEducation
                          : edu
                  )
                : [...educations, editedOrNewEducation]
        );
        setEducationForEditing(null);
    };

    const acceptDeleteWorkExperience = () => {
        if (workKeyForDeletion) {
            setWorkExperiences(
                workExperiences.filter(
                    (work) => workKeyForDeletion !== work.workId
                )
            );
        }
        setWorkKeyForDeletion(null);
    };

    const acceptDeleteEducation = () => {
        if (educationKeyForDeletion) {
            setEducations(
                educations.filter(
                    (education) =>
                        educationKeyForDeletion !== education.educationId
                )
            );
        }
        setEducationKeyForDeletion(null);
    };

    const validate = (values: IMainFormValues) => {
        const errors: Partial<IMainFormValues> = {};

        if (!values.name) {
            errors.name = "Name is required";
        }

        return errors;
    };

    const submitCV = async (
        values: IMainFormValues,
        { setSubmitting }: FormikHelpers<IMainFormValues>
    ) => {
        const dateString = new Date().toString();
        await saveCV(
            { ...values, creationDate: dateString },
            educations,
            workExperiences
        );
        setSubmitting(false);
        const url = await createPDF({
            ...values,
            educations,
            workExperiences,
            creationDate: dateString,
        });
        if (url) {
            setPdfUrl(url);
            history.push("/show-cv");
        }
    };

    return (
        <>
            <WorkExperienceModal
                isOpen={isWorkExperienceModalOpen}
                setHidden={() => setIsWorkExperienceModalOpen(false)}
                addWorkExperience={acceptWorkExperience}
                getWorkKey={generateKey}
                workForEditing={workForEditing}
            />
            <ConfirmationModal
                isOpen={!!workKeyForDeletion}
                title="Delete"
                message={`Delete Work Experience ${
                    workKeyForDeletion
                        ? workExperiences.findIndex(
                              (work) => work.workId === workKeyForDeletion
                          ) + 1
                        : ""
                }?`}
                okButtonText="Delete"
                onOKClick={acceptDeleteWorkExperience}
                onCancelClick={() => setWorkKeyForDeletion(null)}
            />
            <EducationModal
                isOpen={isEduationModalOpen}
                setHidden={() => setIsEducationModalOpen(false)}
                addEducation={acceptEducation}
                getEducationKey={generateKey}
                educationForEditing={educationForEditing}
            />
            <ConfirmationModal
                isOpen={!!educationKeyForDeletion}
                title="Delete"
                message={`Delete Degreee ${
                    educationKeyForDeletion
                        ? educations.findIndex(
                              (edu) =>
                                  edu.educationId === educationKeyForDeletion
                          ) + 1
                        : ""
                }?`}
                okButtonText="Delete"
                onOKClick={acceptDeleteEducation}
                onCancelClick={() => setEducationKeyForDeletion(null)}
            />
            <p>
                {cvData
                    ? `Your CV was last saved on ${cvData.creationDate}`
                    : null}
            </p>
            <Formik
                initialValues={{
                    email: email || "",
                    name: cvData?.name || "",
                    address: cvData?.address || "",
                    phone: cvData?.phone || "",
                    profile: cvData?.profile || "",
                }}
                onSubmit={submitCV}
                validate={validate}
                enableReinitialize={true}
            >
                <Form className="main-form bg-light">
                    <div className="mb-1">
                        <label htmlFor="main-form-email" className="form-label">
                            Email:
                        </label>
                        <Field
                            type="email"
                            className="form-control form-control-sm"
                            name="email"
                            id="main-form-email"
                            placeholder="example@domain.com"
                            readOnly={true}
                        />
                    </div>
                    <div className="mb-1">
                        <label htmlFor="main-form-name" className="form-label">
                            Name:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="name"
                            id="main-form-name"
                            placeholder="Firstname Lastname"
                        />
                        <ErrorMessage name="name">
                            {(msg) => <FormValidationError message={msg} />}
                        </ErrorMessage>
                    </div>
                    <div className="mb-1">
                        <label
                            htmlFor="main-form-address"
                            className="form-label"
                        >
                            Address:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="address"
                            id="main-form-address"
                            placeholder="Street, City, Zip Code"
                        />
                    </div>
                    <div className="mb-1">
                        <label htmlFor="main-form-phone" className="form-label">
                            Phone Number:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="phone"
                            id="main-form-phone"
                            placeholder="123456"
                        />
                    </div>
                    <div className="mb-1">
                        <label
                            htmlFor="main-form-profile"
                            className="form-label"
                        >
                            Profile:
                        </label>
                        <Field
                            as="textarea"
                            className="form-control form-control-sm mb-3"
                            name="profile"
                            rows={3}
                            id="main-form-profile"
                            placeholder="Summary of Your CV"
                        />
                    </div>

                    {educations.map((education, index) => {
                        return (
                            <EducationView
                                key={education.educationId}
                                education={education}
                                educationKey={education.educationId}
                                educationIndex={index}
                                removeEducation={(key: string) =>
                                    setEducationKeyForDeletion(key)
                                }
                                editEducation={editEducation}
                            />
                        );
                    })}
                    <button
                        type="button"
                        className="btn btn-success btn-sm my-2"
                        onClick={() => {
                            setEducationForEditing(null);
                            setIsEducationModalOpen(true);
                        }}
                    >
                        + Add Education
                    </button>
                    <br />
                    {workExperiences.map((work, index) => {
                        return (
                            <WorkExperienceView
                                key={work.workId}
                                work={work}
                                workKey={work.workId}
                                workIndex={index}
                                removeWorkExperience={(key: string) =>
                                    setWorkKeyForDeletion(key)
                                }
                                editWorkExperience={editWorkExperience}
                            />
                        );
                    })}
                    <button
                        type="button"
                        className="btn btn-success btn-sm my-2"
                        onClick={() => {
                            setWorkForEditing(null);
                            setIsWorkExperienceModalOpen(true);
                        }}
                    >
                        + Add Work Experience
                    </button>
                    <br />
                    <button type="submit" className="btn btn-primary">
                        Submit and Create PDF
                    </button>
                </Form>
            </Formik>
        </>
    );
};

export default MainForm;

import React, { useState } from "react";
import Modal from "react-modal";
import { IWorkExperience } from "../types";
import "./WorkExperienceModal.css";
import { Formik, Field, Form, ErrorMessage, FormikHelpers } from "formik";
import FormValidationError from "./FormValidationError";

interface IWorkExperienceModal {
    isOpen: boolean;
    setHidden: () => void;
    workForEditing: IWorkExperience | null;
    addWorkExperience: (work: IWorkExperience, isEditing: boolean) => void;
    getWorkKey: () => string;
}

interface IWorkFormValues {
    employerName: string;
    title: string;
    startDate: string;
    endDate: string;
    workDescription: string;
}

const WorkExperienceModal: React.FC<IWorkExperienceModal> = (props) => {
    const { isOpen, setHidden, addWorkExperience, getWorkKey, workForEditing } =
        props;
    const [formValues, setFormValues] = useState<IWorkFormValues>({
        employerName: "",
        title: "",
        startDate: "",
        endDate: "",
        workDescription: "",
    });

    const afterOpenModal = () => {
        setFormValues(
            workForEditing
                ? {
                      employerName: workForEditing.employerName,
                      title: workForEditing.title,
                      startDate: workForEditing.startDate,
                      endDate: workForEditing.endDate,
                      workDescription: workForEditing.description,
                  }
                : {
                      employerName: "",
                      title: "",
                      startDate: "",
                      endDate: "",
                      workDescription: "",
                  }
        );
    };

    const submitWork = (
        values: IWorkFormValues,
        { setSubmitting }: FormikHelpers<IWorkFormValues>
    ) => {
        const isEditing = !!workForEditing;
        addWorkExperience(
            {
                employerName: values.employerName,
                title: values.title,
                startDate: values.startDate,
                endDate: values.endDate,
                description: values.workDescription,
                workId: workForEditing ? workForEditing.workId : getWorkKey(),
            },
            isEditing
        );
        setSubmitting(false);
        setHidden();
    };

    const validate = (values: IWorkFormValues) => {
        const errors: Partial<IWorkFormValues> = {};

        if (!values.employerName) {
            errors.employerName = "Employer name is required";
        }

        return errors;
    };

    return (
        <Modal
            isOpen={isOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={() => setHidden()}
            className="modal-content"
            contentLabel="Add Work Experience Modal"
            ariaHideApp={false}
            shouldCloseOnOverlayClick={false}
        >
            <h2 className="header">
                {workForEditing ? "Edit" : "Add"} Work Experience
            </h2>

            <Formik
                initialValues={formValues}
                onSubmit={submitWork}
                validate={validate}
                enableReinitialize
            >
                <Form>
                    <div className="mb-1">
                        <label htmlFor="work-form-employerName" className="form-label">
                            Employer:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="employerName"
                            id="work-form-employerName"
                            placeholder="Company Inc"
                        />
                        <ErrorMessage name="employerName">
                            {msg => <FormValidationError message={msg}/>}
                        </ErrorMessage>
                    </div>

                    <div className="mb-1">
                        <label htmlFor="work-form-title" className="form-label">
                            Title:
                        </label>
                        <Field
                            type="text"
                            className="form-control form-control-sm"
                            name="title"
                            id="work-form-title"
                        />
                    </div>

                    <div className="mb-1">
                        <label htmlFor="work-form-startDate" className="form-label">
                            Start Date:
                        </label>
                        <Field
                            type="date"
                            className="form-control form-control-sm"
                            name="startDate"
                            id="work-form-startDate"
                        />
                    </div>

                    <div className="mb-1">
                        <label htmlFor="work-form-endDate" className="form-label">
                            End Date:
                        </label>
                        <Field
                            type="date"
                            className="form-control form-control-sm"
                            name="endDate"
                            id="work-form-endDate"
                        />
                    </div>

                    <div className="mb-1">
                        <label htmlFor="work-form-workDescription" className="form-label">
                            Description:
                        </label>
                        <Field
                            as="textarea"
                            className="form-control form-control-sm"
                            name="workDescription"
                            placeholder="Describe your work"
                            rows={4}
                            id="work-form-workDescription"
                        />
                    </div>

                    <div className="buttons-div mt-2">
                        <button type="submit" className="btn btn-primary">
                            {workForEditing ? "Save" : "Add"}
                        </button>{" "}
                        <button
                            type="button"
                            onClick={() => setHidden()}
                            className="btn btn-secondary"
                        >
                            Cancel
                        </button>
                    </div>
                </Form>
            </Formik>
        </Modal>
    );
};

export default WorkExperienceModal;

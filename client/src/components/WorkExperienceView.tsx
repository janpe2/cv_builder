import React from "react";
import { IWorkExperience } from "../types";
import { iconPencil, iconTrash } from "./Icons";
import "./WorkExperienceView.css";

interface IWorkExperienceView {
    work: IWorkExperience;
    workIndex: number;
    workKey: string;
    removeWorkExperience: (workKey: string) => void;
    editWorkExperience: (workKey: string) => void;
}

const WorkExperienceView: React.FC<IWorkExperienceView> = (props) => {
    const { work, workIndex, workKey, removeWorkExperience, editWorkExperience } = props;

    return (
        <div className="border border-secondary rounded ps-4 pe-4 pb-3 mt-1 mb-1">
            <legend>
                Work Experience {workIndex + 1}{" "}
                <span
                    onClick={() => removeWorkExperience(workKey)}
                    title="Remove work experience"
                    className="action-button"
                    style={{ color: "red" }}
                >
                    {iconTrash}
                </span>
                <span
                    onClick={() => editWorkExperience(workKey)}
                    title="Edit work experience"
                    className="action-button"
                >
                    {iconPencil}
                </span>
            </legend>

            <dl>
                <dt>Employer:</dt>
                <dd>{work.employerName}</dd>

                <dt>Title:</dt>
                <dd>{work.title}</dd>

                <dt>Start Date:</dt>
                <dd>{work.startDate}</dd>

                <dt>End Date:</dt>
                <dd>{work.endDate}</dd>

                <dt>Description:</dt>
                <dd>{work.description}</dd>
            </dl>
        </div>
    );
};

export default WorkExperienceView;

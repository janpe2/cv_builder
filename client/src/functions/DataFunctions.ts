import axios from "axios";
import {
    ICVData,
    ICVDataWithWorkAndEducation,
    IWorkExperience,
    IEducation,
} from "../types";

const baseUrl = "http://localhost:3001/api";

interface ICVDataFetched {
    name: string;
    email: string;
    address: string;
    phone: string;
    profile: string;
    creation_date: string;
    educations: IEducationFetched[];
    work_experiences: IWorkExperienceFetched[];
}

interface IWorkExperienceFetched extends IWorkExperience {
    employer_name: string;
    start_date: string;
    end_date: string;
}

interface IEducationFetched extends IEducation {
    degree_name: string;
    school_name: string;
}

const getCV = async (
    email: string,
    idCounterStart: number
): Promise<{ cv: ICVDataWithWorkAndEducation; nextId: number } | null> => {
    const cv = await axios
        .get<ICVDataFetched>(`${baseUrl}/cvs/${email}`)
        .then((response) => {
            return response.status === 200 ? response.data : null;
        })
        .catch((error) => null);
    if (!cv) {
        return null;
    }

    const workExperiences = await fetchWorkExperiences(email, idCounterStart);
    const firstEducationId = idCounterStart + workExperiences.length;
    const educations = await fetchEducations(email, firstEducationId);

    return {
        cv: {
            name: cv.name,
            email: cv.email,
            address: cv.address,
            phone: cv.phone,
            profile: cv.profile,
            creationDate: convertDateToISO8601(cv.creation_date),
            educations,
            workExperiences,
        },
        nextId: firstEducationId + educations.length,
    };
};

const fetchWorkExperiences = async (
    email: string,
    idCounter: number
): Promise<IWorkExperience[]> => {
    return await axios
        .get<IWorkExperienceFetched[]>(`${baseUrl}/work/${email}`)
        .then((response) => {
            return response.status !== 200
                ? []
                : response.data.map((work, index) => {
                      return {
                          employerName: work.employer_name,
                          title: work.title,
                          startDate: convertDateToISO8601(work.start_date),
                          endDate: convertDateToISO8601(work.end_date),
                          description: work.description,
                          workId: `${idCounter + index}`,
                      };
                  });
        })
        .catch((error) => []);
};

const fetchEducations = async (
    email: string,
    idCounter: number
): Promise<IEducation[]> => {
    return await axios
        .get<IEducationFetched[]>(`${baseUrl}/education/${email}`)
        .then((response) => {
            return response.status !== 200
                ? []
                : response.data.map((education, index) => {
                      return {
                          degreeName: education.degree_name,
                          schoolName: education.school_name,
                          year: education.year,
                          description: education.description,
                          educationId: `${idCounter + index}`,
                      };
                  });
        })
        .catch((error) => []);
};

const saveCV = async (
    cv: ICVData,
    educations: IEducation[],
    workExperiences: IWorkExperience[]
): Promise<boolean> => {
    const workExperiencesSave: IWorkExperienceFetched[] = workExperiences.map(work => {
        return {
            ...work,
            employer_name: work.employerName,
            start_date: convertDateToString(work.startDate),
            end_date: convertDateToString(work.endDate),
        };
    });
    const educationsSave: IEducationFetched[] = educations.map(education => {
        return {
            ...education,
            degree_name: education.degreeName,
            school_name: education.schoolName,
        };
    });
    const cvObject: ICVDataFetched = {
        email: cv.email,
        name: cv.name,
        address: cv.address,
        phone: cv.phone,
        profile: cv.profile,
        creation_date: "",
        work_experiences: workExperiencesSave,
        educations: educationsSave,
    };

    return await axios
        .post<ICVDataFetched>(`${baseUrl}/cvs`, cvObject)
        .then((response) => {
            return response.status === 200;
        })
        .catch((error) => false);
};

const convertDateToISO8601 = (str: string) => {
    const date = new Date(str);
    return Number.isNaN(date.getTime())
        ? ""
        : date.toISOString().substring(0, 10);
};

const convertDateToString = (iso8601: string) => {
    const date = new Date(iso8601);
    return date.toString();
};

const createPDF = async (
    cv: ICVDataWithWorkAndEducation
): Promise<string | null | undefined> => {
    return await axios
        .post<Blob>(`${baseUrl}/pdf`, cv, {
            responseType: "arraybuffer",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/pdf",
            },
        })
        .then((response) => {
            return response.status === 200 && response.data
                ? URL.createObjectURL(
                      new Blob([response.data], { type: "application/pdf" })
                  )
                : null;
        });
};

export { getCV, saveCV, convertDateToISO8601, createPDF };

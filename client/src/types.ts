export interface ICVData {
    name: string;
    email: string;
    address: string;
    phone: string;
    profile: string;
    creationDate: string;
};

export interface IWorkExperience {
    employerName: string;
    title: string;
    startDate: string;
    endDate: string;
    description: string;
    workId: string;
};

export interface IEducation {
    degreeName: string;
    schoolName: string;
    year: number;
    description: string;
    educationId: string;
}

export interface ICVDataWithWorkAndEducation {
    name: string;
    email: string;
    address: string;
    phone: string;
    profile: string;
    creationDate: string;
    educations: IEducation[];
    workExperiences: IWorkExperience[];
};

const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();
const db = require('./queries');

app.use(cors());
app.use(express.json());

const errorHandler = (error, request, response, next) => {
    console.error(error.message);
    next(error);
};

app.get("/api/cvs/:email", db.getCVByEmail);
app.post("/api/cvs", db.createCV);
app.post("/api/pdf", db.createPDF);
app.delete("/api/cvs/:email", db.deleteCV);
app.get("/api/work/:email", db.getWorkExperiencesByEmail);
app.get("/api/education/:email", db.getEducationsByEmail);

const unknownEndpoint = (request, response) => {
    response.status(404).send({ error: "unknown endpoint" });
};

app.use(unknownEndpoint);
app.use(errorHandler);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});

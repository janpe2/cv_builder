const PDFDocument = require("pdfkit");

const bodyFontSize = 14;
const marginWidth = 50;
const headingFontSize = 18;
const headingColor = "blue";
const textColor = "black";
const indentedLeft = 80;

const plainFont = "fonts/SignikaNegative-Regular.ttf";
const boldFont = "fonts/SignikaNegative-Bold.ttf";

const createCvPdf = (request, response) => {
    const {
        email,
        name,
        address,
        phone,
        profile,
        workExperiences,
        educations,
    } = request.body;

    const cvTitleFontSize = 30;
    const cvTitleGap = 0.55;

    const doc = new PDFDocument({ size: "A4" });

    doc.fillColor("#d8d8d8");
    doc.rect(
        marginWidth,
        marginWidth,
        doc.page.width - 2 * marginWidth,
        75
    ).fill();
    doc.fillColor(textColor);

    doc.font(boldFont);
    doc.fontSize(headingFontSize);
    doc.text("CURRICULUM VITAE", marginWidth, marginWidth + 10, { align: "center" });
    doc.moveDown(cvTitleGap)
        .fontSize(cvTitleFontSize)
        .text(name.toUpperCase(), { align: "center" });
    doc.moveTo(marginWidth, doc.y)
        .lineTo(doc.page.width - marginWidth, doc.y)
        .lineWidth(3)
        .strokeColor(headingColor)
        .stroke();

    writePersonalData(email, address, phone, profile, doc);
    writeEducations(educations, doc);
    writeWorkExperiences(workExperiences, doc);

    doc.pipe(response);
    doc.end();
};

const writePersonalData = (email, address, phone, profile, doc) => {
    doc.font(plainFont).fontSize(bodyFontSize);
    writeField("Email", email, doc);
    writeField("Address", address, doc);
    writeField("Phone", phone, doc);
    writeField("Profile", profile, doc);
};

const writeField = (description, value, doc) => {
    const lineGap = 0.75;
    if (value) {
        doc.font(boldFont).fontSize(bodyFontSize);
        doc.moveDown(lineGap);
        doc.text(`${description.toUpperCase()}: `, { continued: true });
        doc.font(plainFont).fontSize(bodyFontSize);
        doc.text(value);
    }
};

const writeEducations = (educations, doc) => {
    const lineGap = 0.75;
    doc.font(boldFont).fontSize(headingFontSize);
    doc.moveDown().fillColor(headingColor).text("EDUCATION");
    doc.moveDown(lineGap).fillColor(textColor);
    doc.fontSize(bodyFontSize);

    educations.forEach((education) => {
        doc.font(boldFont).fontSize(bodyFontSize);
        doc.text(`${education.degreeName || ""}, ${education.year || ""}`);
        doc.font(plainFont).fontSize(bodyFontSize);
        doc.text(education.schoolName || "", indentedLeft, doc.y);
        doc.text(education.description || "", indentedLeft, doc.y);
        doc.x = marginWidth;
        doc.moveDown(lineGap);
    });
};

const writeWorkExperiences = (workExperiences, doc) => {
    const lineGap = 0.75;
    const indent = 30;
    doc.font(boldFont).fontSize(headingFontSize);
    doc.moveDown().fillColor(headingColor).text("WORK EXPERIENCE");
    doc.moveDown(lineGap).fillColor(textColor);
    doc.fontSize(bodyFontSize);

    workExperiences.forEach((work) => {
        doc.font(boldFont).fontSize(bodyFontSize);
        doc.text(`${work.title || ""}, ${work.employerName || ""}`);
        doc.font(plainFont).fontSize(bodyFontSize);
        doc.text(`${work.startDate} — ${work.endDate}`, indentedLeft, doc.y);
        doc.text(work.description || "", indentedLeft, doc.y);
        doc.x = marginWidth;
        doc.moveDown(lineGap);
    });
};

module.exports = { createCvPdf };

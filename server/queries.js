const { Pool } = require("pg");
require("dotenv").config();
const pdf_utils = require('./pdf_utils');

const pool = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DATABASE,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
});
pool.connect((err) => {
    err
        ? console.error("PG connection error", err.stack)
        : console.log("PG connected");
});

const getCVByEmail = (request, response) => {
    const email = request.params.email;
    pool.query("SELECT * FROM cvs WHERE email = $1", [email], (err, res) => {
        if (err) {
            response.status(404).end();
            return;
        }
        res && res.rows && res.rows.length === 0
            ? response.status(404).json({ error: "Email not found" })
            : response.status(200).json(res.rows[0]);
    });
};

const getWorkExperiencesByEmail = (request, response) => {
    const email = request.params.email;
    pool.query(
        `SELECT employer_name, title, start_date, end_date, description FROM cvs
        INNER JOIN works ON works.cv_id = cvs.id
        WHERE cvs.email = $1`,
        [email],
        (err, res) => {
            if (err) {
                response.status(404).end();
                return;
            }
            res && res.rows && res.rows.length === 0
                ? response.status(404).json({ error: "No works not found" })
                : response.status(200).json(res.rows);
        }
    );
};

const getEducationsByEmail = (request, response) => {
    const email = request.params.email;
    pool.query(
        `SELECT degree_name, school_name, year, description FROM cvs
        INNER JOIN educations ON educations.cv_id = cvs.id
        WHERE cvs.email = $1`,
        [email],
        (err, res) => {
            if (err) {
                response.status(404).end();
                return;
            }
            res && res.rows && res.rows.length === 0
                ? response.status(404).json({ error: "No educations found" })
                : response.status(200).json(res.rows);
        }
    );
};

const createCV = (request, response) => {
    const body = request.body;
    const { email } = body;
    if (!email) {
        response.status(400).json({
            error: "Email missing",
        });
    } else {
        pool.query(
            `SELECT id FROM cvs WHERE email = $1`,
            [email],
            (err, res) => {
                err
                    ? response
                          .status(404)
                          .json({ error: `Failed to get id of CV, ${err}` })
                    : res && res.rows && res.rows[0] && res.rows[0].id
                        ? updateCV(request, response, res.rows[0].id)
                        : insertCV(request, response);
            }
        );
    }
};

const insertCV = (request, response) => {
    const body = request.body;
    const {
        email,
        name,
        address,
        phone,
        profile,
        work_experiences,
        educations,
    } = body;
    pool.query(
        `INSERT INTO cvs (email, name, address, phone, profile, creation_date)
        VALUES ($1, $2, $3, $4, $5, NOW())
        RETURNING id`,
        [email, name, address, phone, profile],
        (err, res) => {
            err
                ? response
                      .status(404)
                      .json({ error: `Failed to insert CV, ${err}` })
                : insertWorksAndEducations(
                      work_experiences,
                      educations,
                      res.rows[0].id,
                      response
                  );
        }
    );
};

const updateCV = (request, response, cvId) => {
    const body = request.body;
    const {
        email,
        name,
        address,
        phone,
        profile,
        work_experiences,
        educations,
    } = body;
    
    deleteEducations(cvId);
    deleteWorks(cvId);

    pool.query(
        `UPDATE cvs
        SET email = $1, name = $2, address = $3, phone = $4, profile = $5, creation_date = NOW()
        WHERE email = $1`,
        [email, name, address, phone, profile],
        (err, res) => {
            err
                ? response
                      .status(404)
                      .json({ error: `Failed to update CV, ${err}` })
                : insertWorksAndEducations(
                      work_experiences,
                      educations,
                      cvId,
                      response
                  );
        }
    );
};

const insertWorksAndEducations = (works, educations, cvId, response) => {
    if (works) {
        insertWorks(cvId, works);
    }
    if (educations) {
        insertEducations(cvId, educations);
    }
    response.status(200).end();
};

const insertWorks = (cvId, works) => {
    works.forEach((work) => {
        pool.query(
            `INSERT INTO works (employer_name, title, start_date, end_date, description, cv_id)
            VALUES 
            ($1, $2, $3, $4, $5, $6)`,
            [
                work.employer_name,
                work.title,
                work.start_date,
                work.end_date,
                work.description,
                cvId,
            ],
            (err, res) => {
                err
                    ? console.error("Failed to insert work", err)
                    : console.log("Inserted work OK");
            }
        );
    });
};

const insertEducations = (cvId, educations) => {
    educations.forEach((edu) => {
        pool.query(
            `INSERT INTO educations (degree_name, school_name, year, description, cv_id)
            VALUES 
            ($1, $2, $3, $4, $5)`,
            [edu.degree_name, edu.school_name, edu.year, edu.description, cvId],
            (err, res) => {
                err
                    ? console.error("Failed to insert education", err)
                    : console.log("Inserted education OK");
            }
        );
    });
};

const deleteCV = (request, response) => {
    const email = request.params.email;
    if (!email) {
        return response.status(400).json({
            error: "Email missing",
        });
    }
    pool.query(`DELETE FROM cvs WHERE email = $1`, [email], (err, res) => {
        response.status(err ? 404 : 204).end();
    });
};

const deleteEducations = (cvId) => {
    pool.query(
        `DELETE FROM educations WHERE cv_id = $1`,
        [cvId],
        (err, res) => {}
    );
};

const deleteWorks = (cvId) => {
    pool.query(
        `DELETE FROM works WHERE cv_id = $1`,
        [cvId],
        (err, res) => {}
    );
};

const createPDF = (request, response) => {
    const filename = `${encodeURIComponent(request.body.name)}.pdf`;
    response.setHeader(
        "Content-disposition",
        `attachment; filename="${filename}"`
    );
    response.setHeader("Content-type", "application/pdf");
    pdf_utils.createCvPdf(request, response);
};

module.exports = {
    getCVByEmail,
    createCV,
    deleteCV,
    getWorkExperiencesByEmail,
    getEducationsByEmail,
    createPDF,
};
